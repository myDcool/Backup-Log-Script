#!/bin/bash
d=`date -d "1 day ago" +%F`
log='/var/log/php-fpm/www-error.log'

bakup=${log}'-'${d}

cp $log $bakup

tmp=`tail -50 $log`
echo $tmp > $log

# crontab 0 0 * * * /path/to/script/bakup_log.sh